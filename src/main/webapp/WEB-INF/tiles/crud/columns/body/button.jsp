<%--@elvariable id="column" type="parser.TableBuilder.Column.ButtonColumn"--%>
<%--@elvariable id="item" type="java.lang.Object"--%>
<%--@elvariable id="controllerShortName" type="java.lang.String"--%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<tiles:importAttribute name="column" />
<tiles:importAttribute name="item" />
<tiles:importAttribute name="controllerShortName" />

<c:set var="urlName" value="${controllerShortName}#${column.urlPart}" />

<td style="width: 1%">
    <a href="${s:mvcUrl(urlName).arg(0, item.id).build()}" class="btn btn-xs btn-${column.buttonClass}">
        <i class="glyphicon glyphicon-${column.iconName}"></i>
    </a>
</td>
