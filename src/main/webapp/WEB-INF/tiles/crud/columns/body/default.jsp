<%--@elvariable id="column" type="parser.TableBuilder.Column.ValueColumnInterface"--%>
<%--@elvariable id="item" type="java.lang.Object"--%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<tiles:importAttribute name="column" />
<tiles:importAttribute name="item" />
<td>${item[column.varName]}</td>