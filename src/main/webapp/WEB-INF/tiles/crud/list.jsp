<%--@elvariable id="tableBuilder" type="parser.TableBuilder.TableBuilderInterface"--%>
<%--@elvariable id="controllerShortName" type="java.lang.String"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<c:set var="listUrlName" value="${controllerShortName}#list" />

${s:mvcUrl("SC#list").build()}

<table class="table table-bordered">
    <thead>
    <tr>
        <c:forEach items="${tableBuilder.columns}" var="column">
            <th>
                <tiles:insertDefinition name="${column.headerDefinition}">
                    <tiles:putAttribute name="column" value="${column}"/>
                </tiles:insertDefinition>
            </th>
        </c:forEach>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${tableBuilder.list}" var="item">
        <tr>
            <c:forEach items="${tableBuilder.columns}" var="column">
                <tiles:insertDefinition name="${column.bodyDefinition}">
                    <tiles:putAttribute name="column" value="${column}"/>
                    <tiles:putAttribute name="item" value="${item}"/>
                    <tiles:putAttribute name="controllerShortName" value="${controllerShortName}"/>
                </tiles:insertDefinition>
            </c:forEach>
        </tr>
    </c:forEach>
    </tbody>
</table>