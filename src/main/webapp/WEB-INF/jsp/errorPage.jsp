<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<ul>
<li>path - <c:out value="${path}" /></li>
<li>timestamp - ${timestamp}</li>
<li>status - ${status}</li>
<li>error - ${error}</li>
<li>exception - ${error}</li>
<li>message: <pre><c:out value="${message}" /></pre></li>
<li>errors - ${errors}</li>
<li>trace: <pre><c:out value="${trace}" /></pre></li>
</ul>