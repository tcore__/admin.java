package parser.Entity;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "site")
public class Site extends Object {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "domain")
    private String domain;

    @OneToMany(targetEntity = Page.class, mappedBy = "site")
    private Set<Page> pages = new HashSet<Page>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Set<Page> getPages() {
        return pages;
    }

    public void setPages(Set<Page> pages) {
        this.pages = pages;
    }
}
