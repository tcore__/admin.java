package parser.Entity;


import javax.persistence.*;

@Entity
@Table(name = "page")
public class Page {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "url")
    private String url;

    @ManyToOne(targetEntity = Site.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "site_id", unique = false, nullable = false)
    private Page site;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Page getSite() {
        return site;
    }

    public void setSite(Page site) {
        this.site = site;
    }
}