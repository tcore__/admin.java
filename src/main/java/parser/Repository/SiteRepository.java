package parser.Repository;

import org.springframework.data.repository.CrudRepository;
import parser.Entity.Site;

public interface SiteRepository extends CrudRepository<Site, Long>
{
}
