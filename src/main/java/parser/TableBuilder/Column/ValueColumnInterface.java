package parser.TableBuilder.Column;


public interface ValueColumnInterface extends ColumnInterface {
    String getVarName();
}
