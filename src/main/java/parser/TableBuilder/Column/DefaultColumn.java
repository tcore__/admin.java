package parser.TableBuilder.Column;

public class DefaultColumn implements ValueColumnInterface {
    private String name;

    private String varName;

    public DefaultColumn(String name, String varName) {
        this.name = name;
        this.varName = varName;
    }

    public String getName() {
        return name;
    }

    public DefaultColumn setName(String name) {
        this.name = name;

        return this;
    }

    public String getVarName() {
        return varName;
    }

    public String getHeaderDefinition() {
        return "crud.list.column.header.default";
    }

    public String getBodyDefinition() {
        return "crud.list.column.body.default";
    }
}
