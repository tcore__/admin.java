package parser.TableBuilder.Column;

public abstract class ButtonColumn implements ColumnInterface {
    private String name;

    private String buttonClass;

    private String iconName;

    public ButtonColumn() {
        name = "";
    }

    public String getName() {
        return name;
    }

    public String getHeaderDefinition() {
        return "crud.list.column.header.default";
    }

    public String getBodyDefinition() {
        return "crud.list.column.body.button";
    }

    public String getButtonClass() {
        return buttonClass;
    }

    public void setButtonClass(String buttonClass) {
        this.buttonClass = buttonClass;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public abstract String getUrlPart();
}
