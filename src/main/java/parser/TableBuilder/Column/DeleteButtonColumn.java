package parser.TableBuilder.Column;


import parser.Controller.CrudController;

public class DeleteButtonColumn extends ButtonColumn {
    public DeleteButtonColumn() {
        setButtonClass("danger");
        setIconName("remove");
    }

    public String getUrlPart() {
        return CrudController.DELETE_METHOD_NAME;
    }
}
