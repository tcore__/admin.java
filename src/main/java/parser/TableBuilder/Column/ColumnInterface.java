package parser.TableBuilder.Column;


public interface ColumnInterface {
    String getName();

    String getHeaderDefinition();

    String getBodyDefinition();
}
