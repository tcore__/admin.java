package parser.TableBuilder.Column;


import parser.Controller.CrudController;

public class EditButtonColumn extends ButtonColumn {
    public EditButtonColumn () {
        setButtonClass("primary");
        setIconName("edit");
    }

    public String getUrlPart() {
        return CrudController.EDIT_METHOD_NAME;
    }
}
