package parser.TableBuilder;

import org.springframework.data.repository.CrudRepository;
import parser.TableBuilder.Column.ColumnInterface;

import java.util.Collection;

public interface TableBuilderInterface {
    Collection<ColumnInterface> getColumns();

    TableBuilderInterface setRepository(CrudRepository repository);

    CrudRepository getRepository();

    Iterable getList();
}
