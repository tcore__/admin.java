package parser.TableBuilder.Concrete;

import org.springframework.stereotype.Service;
import parser.TableBuilder.Column.DefaultColumn;
import parser.TableBuilder.Column.ColumnInterface;
import parser.TableBuilder.AbstractTableBuilder;

import java.util.Collection;

@Service
public class SiteTableBuilder extends AbstractTableBuilder {
    public Collection<ColumnInterface> getColumns() {
        Collection<ColumnInterface> collection = super.getColumns();
        collection.add(new DefaultColumn("Domain", "domain"));

        return collection;
    }
}
