package parser.TableBuilder;


import org.springframework.data.repository.CrudRepository;
import parser.TableBuilder.Column.ColumnInterface;
import parser.TableBuilder.Column.DefaultColumn;
import parser.TableBuilder.Column.DeleteButtonColumn;
import parser.TableBuilder.Column.EditButtonColumn;

import java.util.ArrayList;
import java.util.Collection;

public abstract class AbstractTableBuilder implements TableBuilderInterface {
    private CrudRepository repository;

    public TableBuilderInterface setRepository(CrudRepository repository) {
        this.repository = repository;

        return this;
    }

    public CrudRepository getRepository() {
        return repository;
    }

    public Iterable getList() {
        return getRepository().findAll();
    }

    public Collection<ColumnInterface> getColumns() {
        Collection<ColumnInterface> collection = new ArrayList<>();
        collection.add(new DefaultColumn("ID", "id"));
        collection.add(new EditButtonColumn());
        collection.add(new DeleteButtonColumn());

        return collection;
    }
}
