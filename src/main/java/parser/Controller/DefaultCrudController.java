package parser.Controller;


import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import parser.TableBuilder.TableBuilderInterface;
import java.util.HashMap;
import java.util.Map;

public abstract class DefaultCrudController extends CrudController
{
    protected abstract TableBuilderInterface getTableBuilder();
}
