package parser.Controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import parser.Repository.SiteRepository;
import parser.TableBuilder.Concrete.SiteTableBuilder;
import parser.TableBuilder.TableBuilderInterface;

import java.util.HashMap;
import java.util.Map;

@Controller("SSSS")
@RequestMapping("/sites")
public class SiteController extends DefaultCrudController {
    @Autowired
    private SiteRepository repository;

    @Autowired
    private SiteTableBuilder tableBuilder;

    @Override
    protected SiteRepository getRepository() {
        return repository;
    }

    @Override
    protected TableBuilderInterface getTableBuilder() {
        return tableBuilder;
    }

    @RequestMapping(CrudController.LIST_PATH)
    public ModelAndView list() {
        Map<String, Object> params = new HashMap<>();
        params.put("tableBuilder", getTableBuilder());

        return super.list(params);
    }

    @RequestMapping(CrudController.EDIT_PATH)
    public ModelAndView edit(@PathVariable int id) {
        Map<String, Object> params = new HashMap<>();

        return super.edit(params);
    }

    @RequestMapping(CrudController.DELETE_PATH)
    public ModelAndView delete(@PathVariable int id) {
        Map<String, Object> params = new HashMap<>();

        return super.delete(params);
    }
}
