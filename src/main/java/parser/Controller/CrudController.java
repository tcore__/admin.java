package parser.Controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import parser.Service.UrlGenerator;
import parser.TableBuilder.AbstractTableBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public abstract class CrudController {
    public final static String DELETE_PATH = "/delete/{id}";
    public final static String LIST_PATH = "/list";
    public final static String EDIT_PATH = "/edit/{id}";
    public final static String LIST_METHOD_NAME = "list";
    public final static String EDIT_METHOD_NAME = "edit";
    public final static String DELETE_METHOD_NAME = "delete";

    @Autowired
    protected HttpServletRequest request;

    @Autowired
    protected UrlGenerator urlGenerator;

    @Autowired
    private ApplicationContext appContext;

    protected abstract CrudRepository getRepository();

    protected String getShortName() {
        StringBuilder sb = new StringBuilder();
        String simpleTypeName = getClass().getSimpleName();
        for (int i = 0 ; i < simpleTypeName.length(); i++) {
            if (Character.isUpperCase(simpleTypeName.charAt(i))) {
                sb.append(simpleTypeName.charAt(i));
            }
        }
        return sb.toString();
    }

    protected ModelAndView list(Map<String, Object> params) {
        AbstractTableBuilder tableBuilder = (AbstractTableBuilder) params.get("tableBuilder");
        tableBuilder.setRepository(getRepository());

        return new ModelAndView("crud.list", applyDefaultVariables(params));
    }

    protected ModelAndView edit(Map<String, Object> params) {
        return new ModelAndView("crud.edit", applyDefaultVariables(params));
    }

    protected ModelAndView delete(Map<String, Object> params) {
        return new ModelAndView();
    }

    protected Map<String, Object> applyDefaultVariables(Map<String, Object> params) {
        params.put("controllerShortName", getShortName());

        return  params;
    }
}
