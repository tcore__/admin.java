package parser.Service;


import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponentsBuilder;
import parser.Controller.CrudController;

@Service
public class UrlGenerator {
    public String generateListUrl(Class controller) {
        return generateUrl(controller, CrudController.LIST_METHOD_NAME)
                .build().encode().toUriString();
    }

    public String generateEditUrl(Class controller, int id) {
        return  generateUrl(controller, CrudController.EDIT_METHOD_NAME, id).build().encode().toUriString();
    }

    protected UriComponentsBuilder generateUrl(Class controller, String method, Object... argumentValues) {
        return  MvcUriComponentsBuilder.fromMethodName(controller, method, argumentValues);
    }
}
